package clients;

public interface ClientContract extends Comparable<Client> 
{

	// Accessors.
	String getPurchases();
	
	// Transformers
	public int compareTo(Client pivot);
	
	
}
