package clients;

public class Directory {
	
	// A directory can be represented by an array of objects of the following class:
		// class Client {
		// public String firstName;
		// public String lastName;
		// public String contactDetails;
	    // public String purchases;
	    // public String regDate;
		// }
	
	public static void main(String[] args) {
		
// Entries.		
		
		Client entry1 =  new Client();
		entry1.setFirstName ("John");
		entry1.setLastName ("Park");
		entry1.setContactDetails ("21 Falman Drive, JKU OP9");
		entry1.setPurchases("Magz");
		entry1.setRegDate("21/04/08");
		
		Client entry2 =  new Client();
		entry2.setFirstName ("Mark");
		entry2.setLastName ("Reid");
		entry2.setContactDetails ("4 Lords House, KH2 4RT");
		entry2.setPurchases("TV");
		entry2.setRegDate("14/11/05");
		
		Client entry3 =  new Client();
		entry3.setFirstName ("Sarah");
		entry3.setLastName ("fowler");
		entry3.setContactDetails ("88 Soliden Road, IC8 0PL");
		entry3.setPurchases("Radio");
		entry3.setRegDate("30/01/12");
		
		Client entry4 =  new Client();
		entry4.setFirstName ("Aron");
		entry4.setLastName ("Paul");
		entry4.setContactDetails ("41 Harp Lane, IK3 S5D");
		entry4.setPurchases("Paper");
		entry4.setRegDate("02/12/12");
		
		Client entry5 =  new Client();
		entry5.setFirstName ("Annie");
		entry5.setLastName ("Hall");
		entry5.setContactDetails ("77 Green Drive, PXZ 8YH");
		entry5.setPurchases("Web");
		entry5.setRegDate("16/09/08");
		
		Client entry6 =  new Client();
		entry6.setFirstName ("Lucy");
		entry6.setLastName ("Parker");
		entry6.setContactDetails ("112 Oak Cresent, MG7 ULS");
		entry6.setPurchases("Email");
		entry6.setRegDate("19/03/13");
		
		Client entry7 =  new Client();
		entry7.setFirstName ("Danny");
		entry7.setLastName ("Kreg");
		entry7.setContactDetails ("55 Jersy Road, HS8 4J2");
		entry7.setPurchases("Paper");
		entry7.setRegDate("10/09/07");
		
		Client entry8 =  new Client();
		entry8.setFirstName ("Wendy");
		entry8.setLastName ("Lewin");
		entry8.setContactDetails ("60 Kenk Hill, Q2I I7P");
		entry8.setPurchases("TV");
		entry8.setRegDate("27/10/09");
		
// Client[] dir = new Client[4];
// dir[0] = entry1;
// dir[1] = entry2;
// dir[2] = entry3;
// dir[3] = entry4;
		
		// or 
		
		Client[] dir = {entry1, entry2, entry3, entry4, entry5, entry6, entry7, entry8 }; // Adds entries to dir.
		
		String targetPurchases = "Email";
		
		String lastName = searchByPurchases(dir, targetPurchases); // Search dir for lastName.
		
		
		System.out.println(targetPurchases +" - last name found = "+lastName); // Print method and string joining.
		
		
		String[] purchase = searchByLastName(dir, "Parker"); // Searches purchases by lastName.
		for(String purchases : purchase){
			System.out.println("lastname = Parker; purchases = "+purchases); // Print method and string joining.
		}

		System.out.println("print unsorted entries");
		for (Client entry: dir) {
			System.out.println(entry);
		}
		// Print Sorted Entries.
		quickSort(dir, 0, dir.length-1);
		System.out.println("print sorted entries");
		for (Client entry: dir) {
			System.out.println(entry);
		}
		
	}

// static String searchByPurchases (Client[] dir, String targetPurchases);
// Returns the lastName of the (unique) entry in dir whose
// Purchases equals targetPurchases, or null if there is no such entry.
	
	static String searchByPurchases(Client[] dir, String targetPurchases) { // Search by Purchases algorithm.
		int l = 0, r = dir.length - 1;
		while (l <= r) {
			int m = (l + r) / 2;
			int comp = targetPurchases.compareTo(dir[m].getPurchases());
			if (comp == 0)
				return dir[m].getLastName();
			else if (comp < 0)
				r = m - 1;
			else
				// comp > 0
				l = m + 1;
		}
		return null;
	}
	
// Static String[] searchByLastName(Client[] dir, String targetLastName);
// Return an array of all purchases in dir whose lastName equals
// targetLastName, or null if there are no such entries.
	
	static String[] searchByLastName(Client[] dir, String targetLastName) { // Search by LastName algorithm. 
		String[] purchases1 = new String[dir.length];
		int count = 0;
		for (int p = 0; p < dir.length; p++) {
			if (targetLastName.equals(dir[p].getLastName()))
				purchases1[count++] = dir[p].getPurchases();
		}
		if (count == 0)
			return null;
		else {
			String[] purchases2 = new String[count];
			System.arraycopy(purchases1, 0, purchases2, 0, count);
			return purchases2;
		}}
	
// Sort Algorithm.
	
	static void selectionSort (  // Sort method for Client.
			Client[] a, int left, int right) {
		// Sort left to right ascending order.
		for (int l = left; l < right; l++) {
			int p = l;
			Client least = a[p];
			for (int k = l+1; k <= right; k++) {
				int comp = a[k].compareTo(least);
				if (comp < 0) { p = k; least = a[p]; }
			}
			if (p != l) { a[p] = a[l]; a[l] = least; }
		}}

	static void quickSort(Client[] dir, int left, int right) {
		// Sort dir[left...right] into ascending order.
		if (left < right) {
			int p = partition(dir, left, right);
			quickSort(dir, left, p - 1);
			quickSort(dir, p + 1, right);
		}
		
	}
	
	private static int partition (Client[] dir, int left, int right) {
		// Partition dir[left...right] such that
		// dir[left..p-1] are all less than or equal to dir[p], and
		// dir[p+1..right] are all greater than or equal to dir[p].
		// Return p.
		Client half = dir [left];
		int p = left;
		for (int r = left+1; r <= right; r++) {
			int comp = dir [r].compareTo(half);
			if (comp < 0) {
				dir[p] = dir[r];
				dir[r] = dir[p+1];
				dir[p+1] = half;
				p++;
			}
		}
		
		return p;
	}
}
