package clients;

//Client ADT.

public class Client implements ClientContract 
{ // Declaring Strings firstName, lastName, contactDetails, purchases, regDate.
	
	private String firstName;
	private String lastName;
	private String contactDetails;
	private String purchases;
	private String regDate;
	
	public Client(){
		
	}
	
	public Client(String firstName, String lastName, String contactDetails, String purchases, String regDate){ // Used Strings.
		this.firstName = firstName;
		this.lastName = lastName;
		this.contactDetails = contactDetails;
		this.regDate = regDate;
	}
	
	public String getFirstName() { // Get method for firstName.
		return firstName; // Returns firstName
	}
	
	public void setFirstName(String firstName) { // Set method for firstName.
		this.firstName = firstName; // Sets firstName
	}
	
	public String getLastName() { // Get method for lastName.
		return lastName; // Returns lastName.
	}
	
	public void setLastName(String lastName) { // Set method for lastName.
		this.lastName = lastName; // Sets lastName.
	}
	
	public String getContactDetails() { // Get method for contactDetails.
		return contactDetails; // Returns contactDetails.
	}
	
	public void setContactDetails(String contactDetails) { // Set method for contactDetails.
		this.contactDetails = contactDetails; // Sets contactDetails.
	}
	
	public String getPurchases() { // Get method for purchases.
		return purchases; // Returns purchases.
	}
	
	public void setPurchases(String purchases) { // Set method for purchases.
		this.purchases = purchases; // Sets purchases.
	}
	
	public String getRegDate() { // Get method for regDate.
		return regDate; // Returns regDate.
	}
	
	public void setRegDate(String regDate) { // Set method for regDate.
		this.regDate = regDate; // Sets regDate.
	}
	
	public String toString(){ // Joins the strings together.
		return "\tfirstName:" +firstName+"\tlastName:" +lastName+"\tcontactDetails:" +contactDetails+"\tpurchases:" +purchases+"\t\tregDate:" +regDate;
	}
	
    public int compareTo(Client pivot) {
		
		if(this.purchases==null)  {
			
			throw new RuntimeException("A purchase must be set..");
		
		}
		return getPurchases().compareTo(pivot.getPurchases());
	}

}
